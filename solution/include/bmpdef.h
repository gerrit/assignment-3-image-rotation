#ifndef BMPDEF_H_INCLUDED
#define BMPDEF_H_INCLUDED

#include  <stdint.h>
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)
struct pixel { uint8_t b, g, r; };

struct bmp{
    uint64_t w,h;
    struct pixel* img;
};

#endif // BMPDEF_H_INCLUDED
