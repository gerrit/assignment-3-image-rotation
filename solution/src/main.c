#include "bmpdef.h"
#include "import.h"
#include "spin.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc < 4){
        printf("Nuh-uh! 4 arguments reqiured!\n");
        return 0;
    }

    char *from = argv[1];
    char *to = argv[2];
    int spin = atoi(argv[3]);

    size_t ifile_size;
    char *orig_char = readfile(from, &ifile_size);

    //printf("FILE LOADED\n");

    struct bmp orig;
    chartobmp(&orig, orig_char, ifile_size);
    free(orig_char);

    //printf("BMP CONVERTED\n");
    //printf("SPIN: %d\n", spin);

    if (abs(spin) == 270) {spin = -spin%180;}
    while (spin < 0) {spin+=90; spin90ccw(&orig);}
    while (spin > 0) {spin-=90; spin90cw(&orig);}

    //printf("BMP ROTATED\n");

    char *char_spun = NULL;
    size_t ofile_size = 0;
    bmptochar(&orig, &char_spun, &ofile_size);
    free((&orig)->img);

    //printf("FILE CONVERTED\n");

    writefile(to, char_spun, ofile_size);

    //printf("FILE SAVED\n");

    free(char_spun);
    return 0;
}
