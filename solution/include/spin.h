#ifndef SPIN_H_INCLUDED
#define SPIN_H_INCLUDED

#include "bmpdef.h"

void spin90cw (struct bmp *orig);
void spin90ccw (struct bmp *orig);

#endif // SPIN_H_INCLUDED
