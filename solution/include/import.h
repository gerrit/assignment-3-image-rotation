#ifndef IMPORT_H_INCLUDED
#define IMPORT_H_INCLUDED

#include "bmpdef.h"
#include <stdint.h>
#include <stdio.h>

char *readfile(char *file, size_t *len);
short writefile(char *file, char *data, size_t len);
short chartobmp(struct bmp *img, char *data, size_t len);
short bmptochar(struct bmp *img, char **data, size_t *len);

#endif // IMPORT_H_INCLUDED
