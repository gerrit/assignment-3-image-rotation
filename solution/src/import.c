#include "../include/bmpdef.h"
#include <stdio.h>
#include <stdlib.h>
#define l(a,b) ((a)*img->w+(b))
#define BMPDEF 0x4D42

char *readfile(char *file, size_t *len){
    FILE *f = fopen(file, "rb");
    fseek(f, 0, SEEK_END);
    size_t flen = ftell(f);
    *len = flen;
    rewind(f);

    //printf("FILE LENGTH: %d\n", *len);

    char *data = malloc(*len);
    fread(data, 1, *len, f);
    fclose(f);
    return data;
}

short writefile(char *file, char *data, size_t len){
    FILE *f = fopen(file, "wb");
    size_t byte_size = fwrite(data, 1, len, f);
    fclose(f);
    printf("FILE LENGTH IN: %zu, OUT: %zu\n", len, byte_size);
    return 0;
}

short chartobmp(struct bmp *img, char *data, size_t len){
    struct bmp_header head = *(struct bmp_header *)data;
    data += head.bfOffBits;

    img->w = head.biWidth;
    img->h = head.biHeight;
    //printf("WnD: %d, %d\n", head.biWidth, head.biHeight);
    size_t offset = 4-((img->w*3)%4);
    size_t img_size = len - head.bfOffBits;
    img->img = malloc(img_size);

    struct pixel pix;
    //printf("PLACING PIXELS...\n");
    for (size_t i=0; i<img->h; i++){
        for (size_t j=0; j<img->w; j++){
            pix.r = data[offset*i + l(i,j)*3 + 2];
            pix.g = data[offset*i + l(i,j)*3 + 1];
            pix.b = data[offset*i + l(i,j)*3];
            //printf("pad: %d\n", offset*i + l(i,j)*3);
            img->img[l(i,j)] = pix;
        }
    }

    //printf("DONE PLACING PIXELS\n");
    return 0;
}

short bmptochar(struct bmp *img, char **data, size_t *len){
    uint32_t offset = 4-((img->w*3)%4);
    uint32_t img_size = (uint32_t)((img->w*3 + offset)*img->h);
    uint32_t head_size = (uint32_t)sizeof(struct bmp_header);
    uint32_t file_size = img_size + head_size;
    *len = file_size;

    struct bmp_header head = {
        .bfType = BMPDEF,
        .bfileSize = file_size,
        .bfReserved = 0,
        .bfOffBits = head_size,
        .biSize = 40,
        .biWidth = (uint32_t)img->w,
        .biHeight = (uint32_t)img->h,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    char *file_char = calloc(file_size, 1);
    *(struct bmp_header *)file_char = head;
    char *bmp_char = file_char + sizeof(head);
    for (size_t i=0; i<img->h; i++){
        for (size_t j=0; j<img->w; j++){
            *(struct pixel *)(&bmp_char[offset*i + l(i,j)*3]) = img->img[l(i,j)];
        }
    }
    *data = file_char;
    return 0;
}
