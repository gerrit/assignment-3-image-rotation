#include "../include/bmpdef.h"
#include <stdlib.h>
#define lm(a,b,m) ((a)*(m)+(b))

void spin90cw (struct bmp *orig){
    struct bmp spun;
    spun.img = malloc(sizeof(struct pixel)*orig->w*orig->h);
    spun.h = orig->w;
    spun.w = orig->h;

    struct pixel *pixels = orig->img;
    for (size_t i=0; i<orig->h; i++){
        for (size_t j=0; j<orig->w; j++){
            spun.img[lm(spun.h-j-1,i,spun.w)] = pixels[lm(i,j,orig->w)];
        }
    }
    free(orig->img);

    *orig = spun;
}

void spin90ccw (struct bmp *orig){
    struct bmp spun;
    spun.img = malloc(sizeof(struct pixel)*orig->w*orig->h);
    spun.h = orig->w;
    spun.w = orig->h;

    struct pixel *pixels = orig->img;
    for (size_t i=0; i<orig->h; i++){
        for (size_t j=0; j<orig->w; j++){
            spun.img[lm(j,spun.w-i-1,spun.w)] = pixels[lm(i,j,orig->w)];
        }
    }
    free(orig->img);

    *orig = spun;
}
